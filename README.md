# maze-generator

Semestral program in Haskell for NPRG005 (Non-procedural programming) class at Faculty of Mathematics and Physics

## How to run

Use
```
stack exec maze-generator <widht> <height>
```
where widht and height are positive integers specifying the size of required maze.
The maze will be printed to stdout in svg format therefore the most common usage is
```
stack exec maze-generator <width> <height> > maze.svg
```
## How to build

Use 
```
stack build
``` 
and let Stack do its *magic*.
