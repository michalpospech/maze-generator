{-# LANGUAGE OverloadedStrings #-}

module Rendering
  ( renderGridGraph
  ) where

import Data.Graph.Inductive
import qualified Data.Text as DText
import Graphics.Svg

lineThickness = 3

wallColor = "#003B6F"

cellColor = "#F0F0F0"

cellSize = 10

mazeDim :: Int -> Int
mazeDim dim = (dim + 1) * lineThickness + dim * cellSize

renderGridGraph :: Graph g => Int -> Int -> g a b -> Element
renderGridGraph width height g =
  doctype <>
  with
    (svg11_ content)
    [ Version_ <<- "1.1"
    , Width_ <<- intToText (mazeDim width)
    , Height_ <<- intToText (mazeDim height)
    ]
  where
    content =
      rect_ [Width_ <<- "100%", Height_ <<- "100%", Fill_ <<- wallColor] <>
      renderEdges width height g

renderEdges :: Graph g => Int -> Int -> g a b -> Element
renderEdges width height g =
  foldl addConnections mempty [0 .. (width * height - 1)]
  where
    addConnections el num =
      el <> horizontalCorridor g num <> verticalCorridor g num
    horizontalCorridor g num
      | (num + 1) `elem` neighbors g num =
        rect_
          [ X_ <<- intToText (fst $ getCoord num)
          , Y_ <<- intToText (snd $ getCoord num)
          , Width_ <<- intToText (cellSize * 2 + lineThickness)
          , Height_ <<- intToText cellSize
          , Fill_ <<- cellColor
          ]
      | otherwise = mempty
    verticalCorridor g num
      | (num + width) `elem` neighbors g num =
        rect_
          [ X_ <<- intToText (fst $ getCoord num)
          , Y_ <<- intToText (snd $ getCoord num)
          , Width_ <<- intToText cellSize
          , Height_ <<- intToText (cellSize * 2 + lineThickness)
          , Fill_ <<- cellColor
          ]
      | otherwise = mempty
    getCoord num =
      ( lineThickness + mod num width * (cellSize + lineThickness)
      , lineThickness + div num width * (cellSize + lineThickness))
