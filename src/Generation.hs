module Generation
  ( generateMaze
  , generateRandomGraph
  ) where

import Data.Graph.Inductive
import Data.Graph.Inductive.PatriciaTree
import Data.Graph.Inductive.Query.MST
import qualified Data.HashSet as DHash
import Data.Hashable
import System.Random

generateMaze :: Int -> Int -> StdGen -> Gr (Int, Int) Int
generateMaze width height g =
  let graph = generateRandomGraph width height g
      st = msTree graph
      vertices =
        [ (width * y + x, (x, y))
        | x <- [0 .. width - 1]
        , y <- [0 .. height - 1]
        ]
      edges = getEdges st
      emptyGr = buildGr [] :: Gr (Int, Int) Int
      gr = insEdges edges $ insNodes vertices emptyGr
   in gr

getEdges :: LRTree Int -> [LEdge Int]
getEdges tree =
  fst $ foldl foldFunction ([], DHash.empty) (map (listToPairs . unLPath) tree)
  where
    foldFunction (acc, hash) = foldl foldFunction' (acc, hash)
    foldFunction' (acc, hash) pair
      | DHash.member (pairToEdge pair) hash = (acc, hash)
      | otherwise =
        (pairToEdge pair : acc, DHash.insert (pairToEdge pair) hash)

pairToEdge :: (LNode Int, LNode Int) -> LEdge Int
pairToEdge ((node1, label1), (node2, _)) = (node1, node2, label1)

listToPairs :: [a] -> [(a, a)]
listToPairs list = zip list (tail list)

generateRandomGraph :: Int -> Int -> StdGen -> Gr () Int
generateRandomGraph width height gen = undir g
  where
    size = width * height
    vertices =
      [(width * y + x, ()) | x <- [0 .. width - 1], y <- [0 .. height - 1]]
    edges = generateWeights (generateEdges width height 0) gen
    emptyGr = buildGr [] :: Gr () Int
    g = insEdges edges $ insNodes vertices emptyGr

generateEdges :: Int -> Int -> Node -> [Edge]
generateEdges width height num
  | num == width * height - 1 = [] 
  | mod num width == width - 1 = (num, num + width) : rem
  | width * (height - 1) < num = (num, num + 1) : rem
  | otherwise = [(num, num + width), (num, num + 1)] ++ rem
  where
    rem = generateEdges width height (num + 1)

generateWeights :: [Edge] -> StdGen -> [LEdge Int]
generateWeights edges g = zipWith toLEdge edges (randoms g)

