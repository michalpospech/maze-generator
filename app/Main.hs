module Main where

import Generation
import Rendering
import System.Environment
import System.Random

main :: IO ()
main = do
  args <- getArgs
  let (x:y:_) = args
      width = read x :: Int
      height = read y :: Int
  gen <- getStdGen
  let g = generateMaze width height gen
  print $ renderGridGraph width height g
